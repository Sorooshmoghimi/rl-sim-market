from pickle import TRUE
from sys import maxsize
from keras.models import Sequential
from keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Activation, Flatten, LSTM, Bidirectional
from keras.optimizers import adam_v2
from keras.callbacks import TensorBoard
import keras
import tensorflow as tf

from collections import deque
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random
from tqdm import tqdm
import os
from PIL import Image
import cv2

DISCOUNT = 0.99
REPLAY_MEMORY_SIZE = 10_000  # How many last steps to keep for model training
MIN_REPLAY_MEMORY_SIZE = 1_000  # Minimum number of steps in a memory to start training
MINIBATCH_SIZE = 128  # How many steps (samples) to use for training
UPDATE_TARGET_EVERY = 5  # Terminal states (end of episodes)
MODEL_NAME = 'sample_market'
MIN_REWARD = -10  # For model save
MEMORY_FRACTION = 0.20

# Environment settings
EPISODES = 3000

# Exploration settings
epsilon = 0  # not a constant, going to be decayed
EPSILON_DECAY = 0.995
MIN_EPSILON = 0.001

#  Stats settings
AGGREGATE_STATS_EVERY = 1  # episodes
SHOW_PREVIEW = True

SAVE_LAST = False
LOAD_LAST_MODEL = True

class Env:
    TICK_VALUE = 0.00005
    LOOK_BACK = 2
    MAX_STEPS = 50 #5 working days with 1m candles

    OPEN_TRADE_COST = 1 # in usd
    CLOSE_TRADE_COST = 1 # in usd
    WATCH_PENALTY = 0.5
    ACTION_SPACE_SIZE = 3
    OBSERVATION_SPACE_VALUES = 0 

    WATCHING = 0
    TRADING = 1

    HOLD = 0
    BUY = 1
    SELL = 2 
    
    # the dict! (colors) BGR
    colors_d = {
         0: (255, 255, 255),
         1: (0, 255, 0),
         2: (0, 0, 255)
        }

    def __init__(self, df):
        self.df_close_full = df['close'].iloc[1:]
        
        #defferentiating
        self.df_main = df - df.shift(1)
        self.df_main = self.df_main.iloc[1:]

        #convert to numpy
        self.df_main = np.array(self.df_main)
        self.df_close_full = np.array(self.df_close_full)

        self.OBSERVATION_SPACE_VALUES = (df.shape[1])
        self.reset()

    def reset(self):
        init_df_pos = np.random.randint(0, self.df_main.shape[0] - self.MAX_STEPS)
        self.df_ep = self.df_main[init_df_pos: init_df_pos + self.MAX_STEPS]
        self.df_close = self.df_close_full[init_df_pos: init_df_pos + self.MAX_STEPS]
        
        self.pos = 0

        self.current_act = self.WATCHING
        self.current_prof = 0
        self.cum_prof = 0
        self.last_buy_pos = self.pos

        self.buys_pos = []
        self.sells_pos = []

        return np.array(np.append(self.df_ep[self.pos] * 10000, ((self.pos - self.last_buy_pos) / self.MAX_STEPS, self.cum_prof / 110, self.current_prof / 110, self.current_act)))


    def step(self, action):
        reward = 0

        if self.current_act == self.WATCHING:
            self.current_prof = 0
        else:
            self.current_prof = ((self.df_close[self.pos] - self.df_close[self.last_buy_pos]) / self.TICK_VALUE) / 2 - (self.OPEN_TRADE_COST + self.CLOSE_TRADE_COST) # to be edited. 1 tick = 0.5$

        #HOLD
        if action == self.HOLD:
          if self.current_act == self.WATCHING:
            self.last_buy_pos = self.pos
            reward -= self.WATCH_PENALTY

          elif self.current_act == self.TRADING:
            reward += ((self.df_close[self.pos] - self.df_close[self.pos -1]) / self.TICK_VALUE) / 2
          
        #BUY
        elif action == self.BUY:
          if self.current_act == self.WATCHING:
            reward -= self.OPEN_TRADE_COST

            self.last_buy_pos = self.pos
            self.current_act = self.BUY
            self.buys_pos.append(self.pos)
          elif self.current_act == self.TRADING:
            reward += ((self.df_close[self.pos] - self.df_close[self.pos -1]) / self.TICK_VALUE) / 2

        #SELL
        elif action == self.SELL:
            if self.current_act == self.TRADING:
                reward -= self.CLOSE_TRADE_COST
                reward += self.current_prof

                self.cum_prof += self.current_prof 
                self.last_buy_pos = self.pos
                self.current_act = self.WATCHING
                self.current_prof = 0
                self.sells_pos.append(self.pos)
            elif self.current_act == self.WATCHING:
              self.last_buy_pos = self.pos
              reward -= self.WATCH_PENALTY * 2


        self.pos += 1

        new_obs = np.array(np.append(self.df_ep[self.pos] * 10000, ((self.pos - self.last_buy_pos) / self.MAX_STEPS, self.cum_prof / 110, self.current_prof / 110, self.current_act)))
        # print(new_obs)
        done = False
        if self.pos >= self.MAX_STEPS-1 or self.cum_prof < -100 or self.current_prof < -8:
            done = True

            if self.current_act == self.TRADING:
              reward -= self.CLOSE_TRADE_COST
              reward += self.current_prof

              self.cum_prof += self.current_prof 
              self.last_buy_pos = self.pos
              self.current_act = self.WATCHING
              self.current_prof = 0

        return new_obs, reward, done
        

    def render(self):
        max_p = self.df_close.max()
        min_p = self.df_close.min()
        self.TICK_VALUE = 0.00005
        p_steps = round(((max_p - min_p) / self.TICK_VALUE))+1
        
        env = np.zeros((int(p_steps), self.df_ep.shape[0], 3), dtype=np.uint8)

        for i in range(0, self.df_ep.shape[0]):
            if i == self.pos:
                for j in range(0, env.shape[0]):
                    env[j][i] = (80,80,80)

            elif i in self.buys_pos:
                env[int((max_p - self.df_close[i]) / self.TICK_VALUE)][i] = self.colors_d[self.BUY]
            elif i in self.sells_pos:
                env[int((max_p - self.df_close[i]) / self.TICK_VALUE)][i] = self.colors_d[self.SELL]
            else:
                env[int((max_p - self.df_close[i]) / self.TICK_VALUE)][i] = self.colors_d[self.HOLD]
        
        img = cv2.resize(env, (600, 300), interpolation = cv2.INTER_NEAREST)

        cv2.putText(img,f'Profit:{round(self.cum_prof,1)} $', 
            (10,290), #loc
            cv2.FONT_HERSHEY_SIMPLEX, 
            0.5,#font scale
            (255,255,255), #font color
            1, #thickness
            2)#lineType
      
        cv2.imshow('image', np.array(img))  # show it!
        cv2.waitKey(1)

class DQNAgent:
    def __init__(self):

        if LOAD_LAST_MODEL == True:
            self.model = keras.models.load_model('simple_finance/models/last_sample_market.model')
            self.target_model = self.create_model()
            self.target_model.set_weights(self.model.get_weights())
        else:
            #main model
            self.model = self.create_model()
            #target model
            self.target_model = self.create_model()
            # self.target_model.set_weights(self.model.get_weights())

        self.replay_memory = deque(maxlen = REPLAY_MEMORY_SIZE)
        self.target_update_counter = 0

    def create_model(self):
        model = Sequential()
        model.add(Dense(10, activation='tanh', input_shape=(env.df_ep.shape[1] + 4,)))
        model.add(Dropout(0.2))
        model.add(Dense(8))
        model.add(Dropout(0.2))
        model.add(Dense(env.ACTION_SPACE_SIZE, activation='linear'))
        model.compile(loss='mse', optimizer=adam_v2.Adam(learning_rate=0.001), metrics=['accuracy'])
        return model

    def update_replay_memory(self, transition):
        self.replay_memory.append(transition)
    
    def get_qs(self, state):
        return self.model.predict(np.array(state).reshape(-1, *state.shape))[0]

    def train(self, terminal_state, step):
        if len(self.replay_memory) < MIN_REPLAY_MEMORY_SIZE:
            return

        minibacth = random.sample(self.replay_memory, MINIBATCH_SIZE)

        current_state = np.array([transition[0] for transition in minibacth])
        current_qs_list = self.model.predict(current_state)

        new_current_state = np.array([transition[3] for transition in minibacth])
        future_qs_list = self.target_model.predict(new_current_state)

        X = []
        y = []

        for index, (current_state, action, reward, new_current_state, done) in enumerate(minibacth):
            if not done:
                max_future_q = np.max(future_qs_list[index])
                new_q = reward + DISCOUNT * max_future_q
            else:
                new_q = reward

            current_qs = current_qs_list[index]
            current_qs[action] = new_q

            X.append(current_state)
            y.append(current_qs)

        self.model.fit(np.array(X), np.array(y), batch_size=MINIBATCH_SIZE, verbose=0, shuffle=False)

        if terminal_state:
            self.target_update_counter += 1

        if self.target_update_counter > UPDATE_TARGET_EVERY:
            self.target_model.set_weights(self.model.get_weights())
            self.target_update_counter = 0




#Create data frame
SAMPLE_DATA = True

if SAMPLE_DATA == True:
    closes = []
    volumes = []
    for i in range(0,16):
        for inc in range(0,30):
            closes.append(1 + 0.00005 * inc)
            volumes.append(2 + 0.00008 * inc)
        for dec in range(0,30):
            closes.append(1.00145 - 0.00005 * dec)
            volumes.append(2.00240 - 0.00008 * dec)

    raw_df = pd.DataFrame(zip(closes, volumes), columns=['close','volume'])
else:
    raw_df = pd.read_csv('6E_1m_5d.csv', parse_dates=True)

# Create models folder
if not os.path.isdir('simple_finance/models'):
    os.makedirs('simple_finance/models')
            
# plt.plot(raw_df['close'])
# plt.show()

env = Env(raw_df)
agent = DQNAgent()
ep_rewards = [0]

# Iterate over episodes
for episode in tqdm(range(1, EPISODES + 1), ascii=True, unit='episodes'):

    # Restarting episode - reset episode reward and step number
    episode_reward = 0
    step = 1

    # Reset environment and get initial state
    current_state = env.reset()

    # Reset flag and start iterating until episode ends
    done = False
    for i in range(0, env.MAX_STEPS+1):
        if done == True:
            break

        # This part stays mostly the same, the change is to query a model for Q values
        if np.random.random() > epsilon:
            # Get action from Q table
            action = np.argmax(agent.get_qs(current_state))
        else:
            # Get random action
            action = np.random.randint(0, env.ACTION_SPACE_SIZE)

        new_state, reward, done = env.step(action)

        # Transform new continous state to new discrete state and count reward
        episode_reward += reward

        if SHOW_PREVIEW and not episode % AGGREGATE_STATS_EVERY:
            env.render()

        # Every step we update replay memory and train main network
        agent.update_replay_memory((current_state, action, reward, new_state, done))
        agent.train(done, step)

        current_state = new_state
        step += 1

    print('\nreward:', episode_reward, ' - eps:', epsilon)

    # Append episode reward to a list and log stats (every given number of episodes)
    ep_rewards.append(episode_reward)
    if not episode % AGGREGATE_STATS_EVERY or episode == 1:
        average_reward = sum(ep_rewards[-AGGREGATE_STATS_EVERY:])/len(ep_rewards[-AGGREGATE_STATS_EVERY:])
        min_reward = min(ep_rewards[-AGGREGATE_STATS_EVERY:])
        max_reward = max(ep_rewards[-AGGREGATE_STATS_EVERY:])

        if SAVE_LAST == True:
            agent.model.save(f'simple_finance/models/last_{MODEL_NAME}.model')

        # Save model, but only when min reward is greater or equal a set value
        if min_reward >= MIN_REWARD:
            agent.model.save(f'simple_finance/models/best_{MODEL_NAME}__{int(max_reward)}max_{int(average_reward)}avg_{int(min_reward)}min__{int(time.time())}.model')

    # Decay epsilon
    if epsilon > MIN_EPSILON:
        epsilon *= EPSILON_DECAY
        epsilon = max(MIN_EPSILON, epsilon)
